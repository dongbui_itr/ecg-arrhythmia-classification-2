import numpy as np
import os

HES_SAMPLING_RATE = 200
HR_METHOD = 1
NRR = 6
NTACHY = 1
NBRADY = 1
HES_RR_COMPATIBILITY = 0


class TBEvents:
    def __init__(self, tachylimitbpm=120, bradylimitbpm=60):
        # TB
        self.TACHYLIMITBPM = tachylimitbpm
        self.BRADYLIMITBPM = bradylimitbpm
        self.RRs = np.zeros(NRR)
        self.OldBeatSample = 0
        self.NSamples = 0
        self.RRTop = 0
        self.MeanRR = 0

    def set_tachy_limit_bpm(self, tachylimitbpm=120):
        self.TACHYLIMITBPM = tachylimitbpm

    def set_brady_limit_bpm(self, bradylimitbpm=60):
        self.BRADYLIMITBPM = bradylimitbpm

    def tb_detect(self, beat_sample, is_noise):
        """
        Tachycardia and Bradycardia detector function

        :param beat_sample:
        :param is_noise:
        :return:    -3: ini
                    -2: First beat with non-zero sample
                    -1: Beat with same sample
                    0: No Tachy/Brady event is detected
                    2: Bradycardia is detected
                    4: Tachycardia is detected
        """
        tachylimitRR = round(60.0 * HES_SAMPLING_RATE / self.TACHYLIMITBPM)
        bradylimitRR = round(60.0 * HES_SAMPLING_RATE / self.BRADYLIMITBPM)

        if is_noise:
            self.OldBeatSample = 0
            return -3

        if beat_sample != self.OldBeatSample:
            self.NSamples += 1

            if self.OldBeatSample == 0:
                self.OldBeatSample = beat_sample
                return -2

            rr = beat_sample - self.OldBeatSample
            self.OldBeatSample = beat_sample
            self.RRs[self.RRTop] = rr
            self.RRTop += 1
            if self.RRTop >= NRR:
                self.RRTop = 0
        else:
            return -1

        if HR_METHOD == 1:
            if self.NSamples < NRR + 1:
                nSUM = self.NSamples - 1
            else:
                nSUM = NRR

            RRmax = self.RRs[0]
            RRmin = self.RRs[0]
            RRsum = self.RRs[0]

            for i in range(1, nSUM):
                RRsum += self.RRs[i]
                if self.RRs[i] > RRmax:
                    RRmax = self.RRs[i]
                if self.RRs[i] < RRmin:
                    RRmin = self.RRs[i]

            if nSUM < 3:
                RR = (1.0 * RRsum) / nSUM
            else:
                RR = (1.0 * (RRsum - RRmax - RRmin)) / (nSUM - 2)

        self.MeanRR = round(RR)

        if (HES_RR_COMPATIBILITY == 1) and (self.MeanRR > 0):
            self.MeanRR -= 1

        if self.MeanRR > bradylimitRR:
            return 2
        elif self.MeanRR < tachylimitRR:
            return 4
        else:
            return 0
