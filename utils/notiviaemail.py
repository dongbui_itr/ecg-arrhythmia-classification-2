import smtplib
import ssl
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


class NotiForm(object):
    def __init__(self, subject, body, attachment=None):
        """
        n = NotiForm("test eamil", "hello", ["cases/2000404_report_v6/2000404_report_v6_cases_2.json",
                                     "cases/2000404_report_v6/2000404_report_v6_cases_1.json"])
        with open("contacts_file.csv") as file:
            reader = csv.reader(file)
            next(reader)  # Skip header row
            for name, email in reader:
                n.send_email(
                    email
                )

        :param subject:
        :param body:
        :param attachment:
        """
        self.smtp_server = "smtp.gmail.com"
        self.port = 465  # For SSL
        self.sender_email = "ainotiitrvn@gmail.com"
        self.password = "159357ASDW"
        self.subject = subject
        self.body = body
        self.attachment = attachment

    def send_email(self, receiver_email):
        # Create a multipart message and set headers
        message = MIMEMultipart()
        message["From"] = "NOTI FROM ITRVN"
        message["Subject"] = self.subject
        message["Bcc"] = receiver_email  # Recommended for mass emails

        # Add body to email
        message.attach(MIMEText(self.body, "plain"))
        if self.attachment is not None:
            for i, filename in enumerate(self.attachment):
                with open(filename, "rb") as reader:
                    # Add file as application/octet-stream
                    # Email client can usually download this automatically as attachment
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(reader.read())

                    # Encode file in ASCII characters to send by email
                    encoders.encode_base64(part)

                    # Add header as key/value pair to attachment part
                    part.add_header(
                        "Content-Disposition",
                        f"attachment; filename= {filename}",
                    )
                    message.attach(part)

        text = message.as_string()
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:
            server.login(self.sender_email, self.password)
            server.sendmail(self.sender_email, receiver_email, text)
            server.quit()
