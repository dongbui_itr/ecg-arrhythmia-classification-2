import numpy as np
import numpy.fft as fft
from glob import glob
import wfdb as wf
import re
from matplotlib import pyplot as plt
from wfdb.processing import resample_singlechan, resample_ann, resample_sig
from utils.reprocessing import \
    bwr, \
    norm, \
    butter_highpass_filter, \
    smooth, \
    butter_bandpass_filter, \
    butter_lowpass_filter, \
    beat_annotations, \
    afib_annotations
import os
from os import listdir
from os.path import basename, isdir, isfile, join, dirname
import pywt
from scipy import signal
from tensorflow.contrib.util import make_tensor_proto
import tensorflow as tf
from datetime import datetime
from utils import label_aifb as lbs_afib
from sklearn.utils import class_weight
import grpc

# from tensorflow_serving.apis import predict_pb2
# from tensorflow_serving.apis import prediction_service_pb2_grpc

import json
import scipy.misc
import random
import operator
import shutil

NUM_INPUT = 1
NUM_CLASSES = 2
NUM_PREPROCESS_THREADS = 8
MIN_FRACTION_OF_EXAMPLES_IN_QUEUE = 0.4
TYPE_WRITE_BIN = np.int16
MAX_SAMPLE_PROCESS = int(0.2 * 60)
HES_SAMPLING_RATE = 200

NAME_MODEL = 'QRS'
VERSION = 'v0.6.0'
SUB_VER = 0
SAMPLING_RATE = 250
NUM_OVERLAP = float(5.0)

PATH_DATA_TRAINING = '/media/dev/MyFiles/PhysionetData/{}/'
DATA_TRAIN_PATH = '/home/dev/DataForTraining/{}/{}/'.format(NAME_MODEL, VERSION)

NUM_NORMALIZATION = int(0.5 * SAMPLING_RATE)
NUM_POSITIVE_BEAT = int(0.04 * SAMPLING_RATE)
# QRS
NUM_BEFORE_QRS_CANDIDATE = 50
NUM_QRS_MORPHOLOGY = 125
QRS_MODEL_NAME = 'qrs'
QRS_PREDICTION_SIGNATURE = 'qrs_detection'
QRS_INPUT_DATA = 'signal'
QRS_OUTPUT_DATA = 'classes'
# SERVING
DEFAULT_TF_SERVER_NAME = '172.17.0.2'
DEFAULT_TF_SERVER_PORT = 9000


def plot_p_waves(all_file):
    """

    :param all_file:
    :return:
    """
    print('files: {}'.format(len(all_file)))
    for f in all_file:
        print(f)
        if basename(f) == '114':
            channel = 0
        else:
            channel = 2

        record = wf.rdsamp(f, channels=[channel])
        # Avoid cases where the value is NaN
        buf_record = np.nan_to_num(record[0][:, 0])
        fs_origin = record[1].get('fs')
        if fs_origin != SAMPLING_RATE:
            ori_ecg_qrs, _ = resample_sig(buf_record, fs_origin, SAMPLING_RATE)
        else:
            ori_ecg_qrs = buf_record

        study_len = len(ori_ecg_qrs)

        samp_to = 0
        samp_from = 0
        while samp_to < len(ori_ecg_qrs):
            samp_len = min(int(MAX_SAMPLE_PROCESS * SAMPLING_RATE), study_len)
            if samp_len < int(MAX_SAMPLE_PROCESS * SAMPLING_RATE):
                break

            samp_to = samp_from + samp_len
            buf_ecg = ori_ecg_qrs[samp_from: samp_to]

            filter_ecg = butter_bandpass_filter(buf_ecg, 2, 40, SAMPLING_RATE)

            filter_wavelet_ecg = butter_bandpass_filter(buf_ecg, 15, 40, SAMPLING_RATE)

            w = pywt.Wavelet('db2')
            coeffs = pywt.wavedec(filter_wavelet_ecg, w, level=5)
            wavelet_ecg = pywt.waverec(coeffs, w)
            times = np.arange(len(buf_ecg), dtype=float)
            times /= SAMPLING_RATE
            # plt.subplot(211)
            plt.title(basename(f))
            plt.plot(times, filter_ecg)
            # plt.subplot(212)
            plt.plot(times, wavelet_ecg)
            plt.show()
            plt.close()


if __name__ == "__main__":
    EC57 = [['mitdb', 'atr', 'atr'],
            ['escdb', 'atr', 'atr']]

    lst_files = []
    lst_db_names = list()
    for db in EC57:
        lst_db_names.append(db[0])
        files = glob(PATH_DATA_TRAINING.format(db[0]) + '/*.dat')
        files = [p[:-4] for p in files
                 if basename(p)[:-4] not in ['07162', '104', '102', '107', '217', 'bw', 'em', 'ma']
                 if '_200hz' not in basename(p)[:-4]]
        files = np.sort(files)
        if len(lst_files) == 0:
            lst_files = files
        else:
            lst_files = np.concatenate((lst_files, files))

    file_names = glob('/home/dev/DataForTraining/DEG' + '/*.hea')
    # Get rid of the extension
    file_names = [p[:-4] for p in file_names if 'event-' in basename(p)]

    plot_p_waves(file_names)
