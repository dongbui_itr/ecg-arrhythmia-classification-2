import json

labels_json = '{ "(AB": "OR", ' \
              '"(AFL": "OR", ' \
              '"(B": "OR", ' \
              '"(BII": "OR", ' \
              '"(IVR": "OR",' \
              '"(N": "OR",' \
              '"(NOD": "OR",' \
              '"(P": "OR",' \
              '"(PREX": "OR",' \
              '"(SBR": "OR",' \
              '"(SVTA": "OR",' \
              '"(T": "OR",' \
              '"(VFL": "OR",' \
              '"(VT": "OR", ' \
              '"(AFIB": "AF"}'
labels_to_float = '{ "OR": "0", ' \
                  '"AF": "1" }'
float_to_labels = '{ "0": "OR", ' \
                  '"1": "AF" }'

float_to_mit_labels = '{ "0": "0000", ' \
                  '"1": "0800" }'

classes = ['OR', 'AF']

labels = json.loads(labels_to_float)
revert_labels = json.loads(float_to_labels)
revert_mit_labels = json.loads(float_to_mit_labels)
original_labels = json.loads(labels_json)
