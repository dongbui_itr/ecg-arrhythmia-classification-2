from __future__ import (absolute_import, division, print_function,
                        unicode_literals)

import numpy as np
from scipy.signal import butter, filtfilt, lfilter, iirnotch, sosfiltfilt, iirfilter
from scipy.ndimage.filters import maximum_filter1d
import scipy.signal as signal
import warnings
import math

import numpy as np
from scipy.fftpack import hilbert
from scipy.signal import (cheb2ord, cheby2, convolve, get_window, iirfilter,
                          remez)
from pyyawt.dwt1d import waverec, wavedec
from pyyawt.dwt import wmaxlev
from scipy.signal import sosfilt
from scipy.signal import zpk2sos


def beat_annotations(annotation):
    """ Get rid of non-beat markers """
    # good = ['N', 'L', 'R', 'A', 'V', '/', 'a', '!', 'F', 'j', 'f', 'E', 'J', 'e', 'Q', 'S', 'r', 'P', '|']
    good = ["N", "L", "R", "e", "J", "V", "E", "A", "a", "S", "j"]
    ids = np.in1d(annotation.symbol, good)
    samples = annotation.sample[ids]
    symbols = np.asarray(annotation.symbol)[ids]
    return samples, symbols


def afib_annotations(annotation, convert2int=True):
    """ Get rid of non-beat markers """

    event = ['(AB', '(AFL', '(B', '(BII', '(IVR', '(N', '(NOD', '(P', '(PREX', '(SBR', '(SVTA', '(T', '(VFL', '(VT',
             '(AFIB', '~']
    ids = np.in1d(annotation.aux_note, event)
    samples = annotation.sample[ids]
    symbols = np.asarray(annotation.aux_note)[ids]
    return samples, symbols


def bwr(raw, fs, l1=0.2, l2=0.6):
    flen1 = int(l1 * fs / 2)
    flen2 = int(l2 * fs / 2)

    if flen1 % 2 == 0:
        flen1 += 1

    if flen2 % 2 == 0:
        flen2 += 1

    out1 = smooth(raw, flen1)
    out2 = smooth(out1, flen2)
    return raw - out2


from math import sqrt


# Daubechies 4 Constant
c0 = (1 + sqrt(3)) / (4 * sqrt(2))
c1 = (3 + sqrt(3)) / (4 * sqrt(2))
c2 = (3 - sqrt(3)) / (4 * sqrt(2))
c3 = (1 - sqrt(3)) / (4 * sqrt(2))


# -0.23037781330885523
# 0.7148465705525415
# -0.6308807679295904
# -0.02798376941698385

# 0.18703481171888114
# 0.030841381835986965
# -0.032883011666982945
# -0.010597401784997278


def conv(x, h):
    """ Perform the convolution operation between two input signals. The output signal length
    is the sum of the lenght of both input signal minus 1."""
    return np.convolve(x, h)


def db4_dec(x, level=1):
    """ Perform the wavelet decomposition to signal x with Daubechies order 4 basis
    function as many as specified level"""

    # Decomposition coefficient for low pass and high pass
    # Daubechies 4 Constant
    lpk = [c0, c1, c2, c3]
    hpk = [c3, -c2, c1, -c0]

    result = [[]] * (level + 1)
    x_temp = x[:]
    for i in range(level):
        lp = conv(x_temp, lpk)
        hp = conv(x_temp, hpk)

        # Downsample both output by half
        index = np.arange(1, len(lp), 2)
        lp_ds = lp[index]
        hp_ds = hp[index]

        result[level - i] = hp_ds
        x_temp = lp_ds[:]

    result[0] = lp_ds
    return result


def db4_rec(signals, level):
    """ Perform reconstruction from a set of decomposed low pass and high pass signals as deep as specified level"""

    # Reconstruction coefficient
    # Daubechies 4 Constant
    lpk = [c3, c2, c1, c0]
    hpk = [-c0, c1, -c2, c3]

    cp_sig = signals[:]
    for i in range(level):
        lp = cp_sig[0]
        hp = cp_sig[1]

        # Verify new length
        if len(lp) > len(hp):
            length = 2 * len(hp)
        else:
            length = 2 * len(lp)

        # Upsampling by 2
        lpu = np.zeros(length + 1)
        hpu = np.zeros(length + 1)
        index = np.arange(0, length, 2)
        lpu[index] = lp
        hpu[index] = hp
        # Convolve with reconstruction coefficient
        lpc = conv(lpu, lpk)
        hpc = conv(hpu, hpk)

        # Truncate the convolved output by the length of filter kernel minus 1 at both end of the signal
        lpt = lpc[3:-3]
        hpt = hpc[3:-3]

        # Add both signals
        org = lpt + hpt

        if len(cp_sig) > 2:
            cp_sig = [org] + cp_sig[2:]
        else:
            cp_sig = [org]

    return cp_sig[0]


def calcEnergy(x):
    """ Calculate the energy of a signal which is the sum of square of each points in the signal."""
    return np.sum(x * x)


def bwr_dwt(raw, fs, level=4):
    """ Perform the baseline wander removal process against signal raw. The output of this method is signal with correct baseline
        and its baseline """
    en1 = 0
    en2 = 0

    curlp = raw[:]
    num_dec = 0
    while num_dec < level:

        # Decompose 1 level
        [lp, hp] = db4_dec(curlp, 1)

        # Shift and calculate the energy of detail/high pass coefficient
        en0 = en1
        en1 = en2
        en2 = calcEnergy(hp)

        # Check if we are in the local minimum of energy function of high-pass signal
        if en0 > en1 and en1 < en2:
            break

        curlp = lp[:]
        num_dec = num_dec + 1

    # Reconstruct the baseline from this level low pass signal up to the original length
    base = curlp[:]
    for i in range(num_dec):
        base = db4_rec([base, np.zeros(len(base))], 1)

    # Correct the original signal by subtract it with its baseline
    ecg_out = raw - base[:len(raw)]
    # if num_dec == level:
    #     from matplotlib import pyplot as pl
    #     pl.title("level = {}".format(num_dec))
    #     pl.plot(raw + 2.0, label="raw")
    #     pl.plot(ecg_out - 2.0, label="out")
    #     pl.plot(base[:len(raw)], label="base")
    #     pl.legend(loc=4)
    #     pl.show()

    return ecg_out


def bwr_dwt2(raw, fs, fc=0.5):
    L = int(round(np.log2(fs / fc) - 2))
    coeffs, coeffs_len = wavedec(raw, min(L, wmaxlev(len(raw), 'vaidyanathan')), 'vaidyanathan')
    slices = np.cumsum(coeffs_len[:-1])
    coeffs_splited = np.split(coeffs, slices[:-1])
    filtered_coeff = coeffs_splited.copy()
    filtered_coeff[0] = butter_highpass_filter(coeffs_splited[0], fc,
                                               np.shape(coeffs_splited[0])[0] / (len(raw) / fs),
                                               order=1)
    reconstructed_buf = waverec(np.concatenate(filtered_coeff), coeffs_len, 'vaidyanathan')
    return reconstructed_buf


def smooth(x, window_len=11, window='hanning'):
    """smooth the data using a window with requested size.

    This method is based on the convolution of a scaled window with the signal.
    The signal is prepared by introducing reflected copies of the signal
    (with the window size) in both ends so that transient parts are minimized
    in the begining and end part of the output signal.

    input:
        x: the input signal
        window_len: the dimension of the smoothing window; should be an odd integer
        window: the type of window from 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'
            flat window will produce a moving average smoothing.

    output:
        the smoothed signal

    example:

    t=linspace(-2,2,0.1)
    x=sin(t)+randn(len(t))*0.1
    y=smooth(x)

    see also:

    numpy.hanning, numpy.hamming, numpy.bartlett, numpy.blackman, numpy.convolve
    scipy.signal.lfilter

    TODO: the window parameter could be the window itself if an array instead of a string
    NOTE: length(output) != length(input), to correct this: return y[(window_len/2-1):-(window_len/2)] instead of just y.
    """

    if x.ndim != 1:
        raise (ValueError, "smooth only accepts 1 dimension arrays.")

    if x.size < window_len:
        raise (ValueError, "Input vector needs to be bigger than window size.")

    if window_len < 3:
        return x

    if window_len % 2 == 0:
        window_len += 1

    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise (ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'")

    s = np.r_[x[window_len - 1:0:-1], x, x[-2:-window_len - 1:-1]]
    # print(len(s))
    if window == 'flat':  # moving average
        w = np.ones(window_len, 'd')
    else:
        w = eval('np.' + window + '(window_len)')

    y = np.convolve(w / w.sum(), s, mode='valid')
    # output = np.argwhere(np.isnan(y))
    # if len(output) > 0:
    #     print(output)
    return y[int(window_len / 2):-int(window_len / 2)]


def norm(raw, window_len, samp_from=-1, samp_to=-1):
    # The window size is the number of samples that corresponds to the time analogue of 2e = 0.5s
    if window_len % 2 == 0:
        window_len += 1

    abs_raw = abs(raw)
    # Remove outlier
    while True:
        g = maximum_filter1d(abs_raw, size=window_len)
        if np.max(abs_raw) < 5.0:
            break

        abs_raw[g > 5.0] = 0

    g_smooth = smooth(g, window_len, window='hamming')
    g_mean = np.mean(g_smooth) / 2.0
    g_smooth = np.clip(g_smooth, g_mean, None)
    # Avoid cases where the value is )
    g_smooth[g_smooth < 0.01] = 1
    normalized = np.divide(raw, g_smooth)

    # if samp_from < 18986081 < samp_to:
    #     print(samp_from)
    #     from matplotlib import pyplot as plt
    #     plt.plot(raw, label="raw")
    #     plt.plot(g_smooth, label="g_smooth")
    #     plt.legend(loc=4)
    #     plt.show()
    #     plt.close()

    return normalized


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def butter_notch_filter(x, fscut, fs, Q=30.0):
    w0 = fscut / (fs / 2)  # Normalized Frequency
    # Design notch filter
    b, a = iirnotch(w0, Q)
    y = filtfilt(b, a, x)
    return y


def butter_highpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='high', analog=False)
    return b, a


def butter_highpass_filter(data, cutoff, fs, order=5):
    b, a = butter_highpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def multi_butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y0 = filtfilt(b, a, data[:, 0])
    y1 = filtfilt(b, a, data[:, 1])
    y2 = filtfilt(b, a, data[:, 2])
    return np.vstack((y0, y1, y2)).T


def iir_bandpass(data, freqmin, freqmax, df, corners=4, zerophase=True):
    """
    :copyright:
    The ObsPy Development Team (devs@obspy.org)

    Butterworth-Bandpass Filter.

    Filter data from ``freqmin`` to ``freqmax`` using ``corners``
    corners.
    The filter uses :func:`scipy.signal.iirfilter` (for design)
    and :func:`scipy.signal.sosfilt` (for applying the filter).

    :type data: numpy.ndarray
    :param data: Data to filter.
    :param freqmin: Pass band low corner frequency.
    :param freqmax: Pass band high corner frequency.
    :param df: Sampling rate in Hz.
    :param corners: Filter corners / order.
    :param zerophase: If True, apply filter once forwards and once backwards.
        This results in twice the filter order but zero phase shift in
        the resulting filtered trace.
    :return: Filtered data.
    """
    fe = 0.5 * df
    low = freqmin / fe
    high = freqmax / fe
    # raise for some bad scenarios
    if high - 1.0 > -1e-6:
        msg = ("Selected high corner frequency ({}) of bandpass is at or "
               "above Nyquist ({}). Applying a high-pass instead.").format(
            freqmax, fe)
        warnings.warn(msg)
        return highpass(data, freq=freqmin, df=df, corners=corners,
                        zerophase=zerophase)
    if low > 1:
        msg = "Selected low corner frequency is above Nyquist."
        raise ValueError(msg)
    z, p, k = iirfilter(corners, [low, high], btype='band',
                        ftype='butter', output='zpk')
    sos = zpk2sos(z, p, k)
    if zerophase:
        firstpass = sosfilt(sos, data)
        return sosfilt(sos, firstpass[::-1])[::-1]
    else:
        return sosfilt(sos, data)


def highpass(data, freq, df, corners=4, zerophase=False):
    """
    Butterworth-Highpass Filter.

    Filter data removing data below certain frequency ``freq`` using
    ``corners`` corners.
    The filter uses :func:`scipy.signal.iirfilter` (for design)
    and :func:`scipy.signal.sosfilt` (for applying the filter).

    :type data: numpy.ndarray
    :param data: Data to filter.
    :param freq: Filter corner frequency.
    :param df: Sampling rate in Hz.
    :param corners: Filter corners / order.
    :param zerophase: If True, apply filter once forwards and once backwards.
        This results in twice the number of corners but zero phase shift in
        the resulting filtered trace.
    :return: Filtered data.
    """
    fe = 0.5 * df
    f = freq / fe
    # raise for some bad scenarios
    if f > 1:
        msg = "Selected corner frequency is above Nyquist."
        raise ValueError(msg)
    z, p, k = iirfilter(corners, f, btype='highpass', ftype='butter',
                        output='zpk')
    sos = zpk2sos(z, p, k)
    if zerophase:
        firstpass = sosfilt(sos, data)
        return sosfilt(sos, firstpass[::-1])[::-1]
    else:
        return sosfilt(sos, data)


def eclipse_distance(a, b):
    return math.sqrt(math.pow((a - b), 2))


def agglomerative_clustering(labels, fs):
    positions = np.where(labels == 1)[0]
    groups = []
    groups_len = []
    if len(positions) > 0:
        groups_temp = [positions[0]]
        for index in range(1, len(positions)):
            if eclipse_distance(positions[index], groups_temp[-1]) > 0.080 * fs:
                beat_position = int(np.mean(groups_temp))
                groups.append(beat_position)
                groups_len.append(len(groups_temp))

                groups_temp.clear()

            groups_temp.append(positions[index])

        if len(groups_temp) > 0:
            groups.append(int(np.mean(groups_temp)))
            groups_len.append(len(groups_temp))
        groups = np.asarray(groups)
        groups_len = np.asarray(groups_len)

    return groups, groups_len


def calculate_slope(ecg, index_beat, fs):
    # mean slope of the waveform at that position
    slope = np.mean(np.diff(ecg[index_beat - round(0.075 * fs):index_beat]))
    return slope


def is_t_wave(ecg, peak, repeak, fs, qrs_radius=0.05):
    segment_slope = np.rad2deg(np.arctan2((ecg[peak] - ecg[peak - int(qrs_radius * fs)]), int(qrs_radius * fs)))
    last_qrs_slope = np.rad2deg(np.arctan2((ecg[repeak] - ecg[repeak - int(qrs_radius * fs)]), int(qrs_radius * fs)))

    # Should we be using absolute values?
    if abs(segment_slope) <= 0.5 * abs(last_qrs_slope):
        return True
    else:
        return False


def select_beat_from_group(beats, groups_len, ecg, fs, num_before_qrs_candidate, min_rr):
    beats = np.add(beats, num_before_qrs_candidate)
    selected_beats = []
    re_peak = beats[0]
    re_peak_len = groups_len[0]
    selected_beats.append(beats[0])
    for i in range(1, len(beats)):
        peak = beats[i]
        if abs(peak - re_peak) < 0.36 * fs and is_t_wave(ecg, peak, re_peak, fs):
            continue

        if groups_len[i] < 5:
            continue

        if peak - re_peak > (min_rr * fs):
            re_peak = peak
            re_peak_len = groups_len[i]
            selected_beats.append(peak)
        elif groups_len[i] > re_peak_len:
            re_peak = peak
            re_peak_len = groups_len[i]
            selected_beats[-1] = peak

    return np.asarray(selected_beats)
