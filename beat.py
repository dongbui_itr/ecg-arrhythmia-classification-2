import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


def conv1d(x, num_filters, kernel_size, strides=1, pad='same', act=True, bn=True, is_training=True):
    x = tf.keras.layers.Conv1D(kernel_size=kernel_size, filters=num_filters, strides=strides,
                               padding=pad, trainable=is_training)(x)
    if bn:
        x = tf.keras.layers.BatchNormalization(trainable=is_training)(x)

    if act:
        x = tf.keras.layers.ReLU(trainable=is_training)(x)

    return x


def conv2d(x, num_filters, kernel_size, strides=1, pad='same', act=True, bn=True, is_training=True):
    x = tf.keras.layers.Conv2D(kernel_size=kernel_size, filters=num_filters, strides=strides,
                               padding=pad, trainable=is_training)(x)

    if bn:
        x = tf.keras.layers.BatchNormalization(trainable=is_training)(x)

    if act:
        x = tf.keras.layers.ReLU(trainable=is_training)(x)

    return x


def build(name,
          inputs,
          is_training,
          num_of_class=None,
          feature_len=-1,
          feature_row=-1,
          feature_col=-1,
          initial_bias=None):
    if name == 'beat_net' and feature_len > 0:
        return beat_net(inputs, is_training, num_of_class)
    elif name == 'beat_seq' and feature_len > 0:
        return beat_seq(inputs, is_training, num_of_class)
    elif name == 'beat_seq2' and feature_len > 0:
        return beat_seq2(inputs, is_training, num_of_class)
    elif name == 'beat_seq3' and feature_len > 0:
        return beat_seq3(inputs, is_training, num_of_class)


l2_factor = 0.01
regularize = tf.keras.regularizers.L2(l2_factor)


def conv_block(_inputs, filters, kernel_size, stride, dropout_rate=0.5):

    x = tf.keras.layers.BatchNormalization()(_inputs)
    x = tf.keras.layers.ReLU()(x)
    x = tf.keras.layers.Dropout(dropout_rate)(x)

    return tf.keras.layers.Conv1D(filters=filters, kernel_size=kernel_size, strides=stride,
                                  padding='same',
                                  # kernel_regularizer=regularize
                                  )(x)


def residual_block(_inputs, filters, _kernel, num_block, dropout_rate=0.5):
    x = conv_block(_inputs, filters, kernel_size=_kernel, stride=1, dropout_rate=dropout_rate)
    x = tf.keras.layers.MaxPool1D(pool_size=3, strides=2, padding='same')(x)
    x = conv_block(x, filters, kernel_size=_kernel, stride=1, dropout_rate=dropout_rate)
    y = tf.keras.layers.Conv1D(filters=filters, kernel_size=1, strides=1, padding='same',
                               # kernel_regularizer=regularize
                               )(_inputs)
    y = tf.keras.layers.MaxPool1D(pool_size=3, strides=2, padding='same')(y)
    y = tf.keras.layers.Add()([x, y])
    for _ in range(num_block):
        x = conv_block(y, filters, kernel_size=_kernel, stride=1, dropout_rate=dropout_rate)
        x = conv_block(x, filters, kernel_size=_kernel, stride=1, dropout_rate=dropout_rate)
        y = tf.keras.layers.Add()([x, y])
    return y


def rhythm_net(inputs, is_training, num_of_class, dropout_rate=0.5, kernel=3):
    """

    :param kernel:
    :param dropout_rate:
    :param inputs: 15,360 samples (60 seconds, data were resampled to 256 Hz)
    :param is_training:
    :param num_of_class:
    :return:
    """
    num_filters_rhythm_net = [16, 32, 48, 64, 80, 96, 112]
    num_block = 9

    input_layer = tf.keras.layers.Input(shape=(inputs, 1))
    block_layer = tf.keras.layers.Conv1D(filters=16, kernel_size=kernel, strides=2, padding='same')(input_layer)
    for i in range(len(num_filters_rhythm_net)):
        block_layer = residual_block(block_layer, num_filters_rhythm_net[i], _kernel=kernel,
                                     dropout_rate=dropout_rate, num_block=num_block)

    block_layer = tf.keras.layers.Dense(num_of_class)(block_layer)
    softmax_layer = tf.keras.layers.Softmax(axis=-1)(block_layer)

    return tf.keras.Model(input_layer, softmax_layer, name='BeatNet')


def beat_net(inputs, is_training, num_of_class, dropout_rate=0.5, kernel=3):
    num_filters_rhythm_net = [16, 32, 48, 64]
    num_block = 7

    input_layer = tf.keras.layers.Input(shape=(inputs, 1))
    block_layer = tf.keras.layers.Conv1D(filters=16, kernel_size=kernel, strides=2, padding='same')(input_layer)
    for i in range(len(num_filters_rhythm_net)):
        block_layer = residual_block(block_layer, num_filters_rhythm_net[i], _kernel=kernel,
                                     dropout_rate=dropout_rate, num_block=num_block)
    # block_layer = tf.keras.layers.BatchNormalization()(block_layer)
    # block_layer = tf.keras.layers.ReLU()(block_layer)
    # block_layer = tf.keras.layers.Dropout(dropout_rate)(block_layer)
    block_layer = tf.keras.layers.ZeroPadding1D()(block_layer)
    block_layer = tf.keras.layers.Concatenate()([
        tf.keras.layers.Cropping1D((0, 2))(block_layer),
        tf.keras.layers.Cropping1D((1, 1))(block_layer),
        tf.keras.layers.Cropping1D((2, 0))(block_layer)
    ])
    block_layer = tf.keras.layers.Dense(num_of_class)(block_layer)
    softmax_layer = tf.keras.layers.Softmax(axis=-1)(block_layer)

    return tf.keras.Model(input_layer, softmax_layer, name='BeatNet')


def beat2(inputs, num_of_class):
    model = tf.keras.models.Sequential()
    channels = [32, 32, 64, 64, 128, 128]
    model.add(tf.keras.layers.InputLayer(input_shape=(inputs, 1)))
    for channel in channels:
        model.add(tf.keras.layers.Conv1D(kernel_size=5, filters=channel, strides=1, padding='same', activation='elu'))
        model.add(tf.keras.layers.MaxPool1D(pool_size=3, strides=2))
    model.add(tf.keras.layers.Flatten())
    model.add(tf.keras.layers.Dense(units=256, activation='elu'))
    model.add(tf.keras.layers.Dropout(rate=0.5))
    model.add(tf.keras.layers.Dense(units=num_of_class))
    model.summary()


def beat_seq(inputs, is_training, num_of_class, dropout_rate=0.5, kernel=7):
    """

    :param kernel:
    :param dropout_rate:
    :param inputs: 15,360 samples (60 seconds, data were resampled to 256 Hz)
    :param is_training:
    :param num_of_class:
    :return:
    """
    num_filters_rhythm_net = [16, 32, 48, 64]
    num_block = 7

    input_layer = tf.keras.layers.Input(shape=(inputs, 1))
    block_layer = tf.keras.layers.Conv1D(filters=16, kernel_size=kernel, strides=2,
                                         padding='same', kernel_regularizer='l2')(input_layer)

    for i in range(len(num_filters_rhythm_net)):
        block_layer = residual_block(block_layer, num_filters_rhythm_net[i], _kernel=kernel,
                                     dropout_rate=dropout_rate, num_block=num_block)
    lstm_outputs = tf.keras.layers.Bidirectional(
        tf.keras.layers.LSTM(units=64, dropout=dropout_rate, return_sequences=True))(block_layer)
    softmax_layer = tf.keras.layers.Dense(num_of_class)(lstm_outputs)
    softmax_layer = tf.keras.layers.Softmax(axis=-1)(softmax_layer)

    return tf.keras.Model(input_layer, softmax_layer, name='BeatSequence')


class Transformer(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.1):
        super(Transformer, self).__init__()
        self.attention = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim, dropout=rate)
        self.ffn = keras.Sequential(
            [
                layers.Dense(ff_dim, activation="relu"),
                layers.Dense(embed_dim)
            ]
        )
        self.normalize1 = layers.LayerNormalization(epsilon=1e-6)
        self.normalize2 = layers.LayerNormalization(epsilon=1e-6)
        self.dropout1 = layers.BatchNormalization()
        self.dropout2 = layers.BatchNormalization()

    def call(self, inputs, **kwargs):
        attn_output = self.attention(inputs, inputs)
        attn_output = self.dropout1(attn_output, training=kwargs['training'])
        out1 = self.normalize1(inputs + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=kwargs['training'])
        return self.normalize2(out1 + ffn_output)


def beat_seq2(inputs, is_training, num_of_class, dropout_rate=0.5, kernel=3):
    """

    :param kernel:
    :param dropout_rate:
    :param inputs: 15,360 samples (60 seconds, data were resampled to 256 Hz)
    :param is_training:
    :param num_of_class:
    :return:
    """
    num_filters_rhythm_net = [16, 32, 48, 64]
    num_block = 7

    input_layer = tf.keras.layers.Input(shape=(inputs, 1))
    block_layer = tf.keras.layers.Conv1D(filters=16, kernel_size=kernel, strides=2, padding='same')(input_layer)
    for i in range(len(num_filters_rhythm_net)):
        block_layer = residual_block(block_layer, num_filters_rhythm_net[i], _kernel=kernel,
                                     dropout_rate=dropout_rate, num_block=num_block)
    block_layer = tf.keras.layers.ZeroPadding1D()(block_layer)
    block_layer = tf.keras.layers.Concatenate()([
        tf.keras.layers.Cropping1D((0, 2))(block_layer),
        tf.keras.layers.Cropping1D((1, 1))(block_layer),
        tf.keras.layers.Cropping1D((2, 0))(block_layer)
    ])
    transformer_output = Transformer(64*3, 8, 256)(block_layer)

    softmax_layer = tf.keras.layers.Dense(num_of_class)(transformer_output)
    softmax_layer = tf.keras.layers.Softmax(axis=-1)(softmax_layer)
    print('Transformer')
    return tf.keras.Model(input_layer, softmax_layer, name='BeatSequence2')


def beat_seq3(inputs, is_training, num_of_class, dropout_rate=0.5, kernel=3):
    """

    :param kernel:
    :param dropout_rate:
    :param inputs: 15,360 samples (60 seconds, data were resampled to 256 Hz)
    :param is_training:
    :param num_of_class:
    :return:
    """
    num_filters = [16, 32, 48, 64]
    num_block = 7

    input_layer = tf.keras.layers.Input(shape=(inputs, 1))
    block_layer = tf.keras.layers.Conv1D(filters=16, kernel_size=kernel, strides=2, padding='same')(input_layer)
    for i in range(len(num_filters)):
        block_layer = residual_block(block_layer, num_filters[i],
                                     _kernel=kernel, num_block=num_block, dropout_rate=dropout_rate)
    # block_layer = tf.keras.layers.ZeroPadding1D()(block_layer)
    # block_layer = tf.keras.layers.Concatenate()([
    #     tf.keras.layers.Cropping1D((0, 2))(block_layer),
    #     tf.keras.layers.Cropping1D((1, 1))(block_layer),
    #     tf.keras.layers.Cropping1D((2, 0))(block_layer)
    # ])
    transformer_output = Transformer(64, 4, 128, rate=dropout_rate)(block_layer)
    transformer_output = Transformer(64, 4, 128, rate=dropout_rate)(transformer_output)
    transformer_output = Transformer(64, 4, 128, rate=dropout_rate)(transformer_output)
    # transformer_output = Transformer(64, 8, 128, rate=dropout_rate)(transformer_output)
    # transformer_output = Transformer(64, 8, 128, rate=dropout_rate)(transformer_output)

    softmax_layer = tf.keras.layers.Dense(num_of_class)(transformer_output)
    softmax_layer = tf.keras.layers.Softmax(axis=-1)(softmax_layer)
    print('Transformer')
    return tf.keras.Model(input_layer, softmax_layer, name='BeatSequence2')


if __name__ == '__main__':
    model = beat_seq3(60*256, True, 4, kernel=3)
    model.summary()
    # print(tf.config.list_physical_devices('GPU'))
