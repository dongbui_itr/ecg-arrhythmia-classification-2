Model path: arrhythmia_model/arrhythmia.128.750.v0.2.0.k0
Batch size : 128
Total training samples : 530970
---  training ---
	2019-05-11 20:31:57.716017: step 0, loss = 5.1861 (2562.7 examples/sec; 0.050 sec/batch)
	2019-05-11 20:32:17.522413: step 1000, loss = 2.6024 (6462.6 examples/sec; 0.020 sec/batch)
	2019-05-11 20:32:38.860995: step 2000, loss = 1.9249 (5998.5 examples/sec; 0.021 sec/batch)
	2019-05-11 20:32:54.911445: step 3000, loss = 1.3869 (7974.8 examples/sec; 0.016 sec/batch)
	2019-05-11 20:33:02.635894: step 4000, loss = 1.1010 (16570.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:10.368350: step 5000, loss = 0.9701 (16553.6 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:18.103078: step 6000, loss = 0.6648 (16548.7 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:25.867039: step 7000, loss = 0.5196 (16487.0 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:33.579093: step 8000, loss = 0.4185 (16596.8 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:41.275690: step 9000, loss = 0.3206 (16631.4 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:49.013972: step 10000, loss = 0.3166 (16540.5 examples/sec; 0.008 sec/batch)
	2019-05-11 20:33:56.781439: step 11000, loss = 0.2289 (16479.0 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:04.493170: step 12000, loss = 0.1563 (16598.7 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:12.184060: step 13000, loss = 0.1976 (16642.5 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:19.924216: step 14000, loss = 0.1027 (16537.7 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:27.688297: step 15000, loss = 0.0775 (16486.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:35.449232: step 16000, loss = 0.0893 (16492.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:43.169976: step 17000, loss = 0.0947 (16578.1 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:50.874772: step 18000, loss = 0.0422 (16613.6 examples/sec; 0.008 sec/batch)
	2019-05-11 20:34:58.599051: step 19000, loss = 0.0347 (16570.5 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:06.373711: step 20000, loss = 0.0426 (16464.4 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:14.083558: step 21000, loss = 0.0388 (16601.5 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:21.792864: step 22000, loss = 0.0457 (16603.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:29.525376: step 23000, loss = 0.0190 (16552.8 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:37.302278: step 24000, loss = 0.0259 (16459.0 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:45.040066: step 25000, loss = 0.0334 (16542.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:35:52.761938: step 26000, loss = 0.0353 (16576.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:00.500672: step 27000, loss = 0.0119 (16539.5 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:08.320714: step 28000, loss = 0.0146 (16368.8 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:16.023810: step 29000, loss = 0.0144 (16616.6 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:23.763812: step 30000, loss = 0.0101 (16536.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:31.501182: step 31000, loss = 0.0087 (16543.7 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:39.311334: step 32000, loss = 0.0136 (16388.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:47.018307: step 33000, loss = 0.1035 (16608.3 examples/sec; 0.008 sec/batch)
	2019-05-11 20:36:54.753931: step 34000, loss = 0.0083 (16546.9 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:02.499307: step 35000, loss = 0.0256 (16526.1 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:10.289037: step 36000, loss = 0.0707 (16431.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:18.026849: step 37000, loss = 0.0721 (16542.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:25.774650: step 38000, loss = 0.0077 (16521.4 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:33.511428: step 39000, loss = 0.0149 (16543.7 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:41.252748: step 40000, loss = 0.0073 (16535.2 examples/sec; 0.008 sec/batch)
	2019-05-11 20:37:48.984458: step 41000, loss = 0.0094 (16554.6 examples/sec; 0.008 sec/batch)
---  validation ---
	se 	{'PVC': 0.981404958677686, 'NOR': 0.9993328549541338, 'APC': 0.8613756613756614}
	+p 	{'PVC': 0.9831460674157303, 'NOR': 0.9992030203782454, 'APC': 0.881429344883595}
Last iterations 41480
---  testing full (mit+aha+esc+nst) ---
	se 	{'PVC': 0.9864352008416939, 'NOR': 0.9994988455028896, 'APC': 0.8927344782034347}
	+p 	{'PVC': 0.9883291920789097, 'NOR': 0.9993971250035135, 'APC': 0.9056553202894666}
	acc 	{'PVC': 0.9993681827769073, 'NOR': 0.9989275114499216, 'APC': 0.9992862631071472}
---  finish ---
