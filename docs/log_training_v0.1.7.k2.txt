Model path: arrhythmia_model/arrhythmia.128.750.v0.1.7.k2
Batch size : 128
Total training samples : 530970
---  training ---
	2019-05-09 07:44:36.464299: step 0, loss = 13.9027 (7525.9 examples/sec; 0.017 sec/batch)
	2019-05-09 07:44:50.536710: step 1000, loss = 9.1543 (9095.7 examples/sec; 0.014 sec/batch)
	2019-05-09 07:45:04.378122: step 2000, loss = 7.0764 (9247.6 examples/sec; 0.014 sec/batch)
	2019-05-09 07:45:18.265941: step 3000, loss = 5.6640 (9216.7 examples/sec; 0.014 sec/batch)
	2019-05-09 07:45:32.110084: step 4000, loss = 4.3140 (9245.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:45:45.909434: step 5000, loss = 3.3576 (9276.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:45:59.773472: step 6000, loss = 2.5896 (9232.3 examples/sec; 0.014 sec/batch)
	2019-05-09 07:46:13.742265: step 7000, loss = 2.0398 (9163.3 examples/sec; 0.014 sec/batch)
	2019-05-09 07:46:27.524864: step 8000, loss = 1.5831 (9287.1 examples/sec; 0.014 sec/batch)
	2019-05-09 07:46:41.367148: step 9000, loss = 1.3661 (9247.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:46:55.149843: step 10000, loss = 0.9753 (9287.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:47:09.070590: step 11000, loss = 0.7553 (9194.7 examples/sec; 0.014 sec/batch)
	2019-05-09 07:47:22.990853: step 12000, loss = 0.5930 (9195.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:47:36.816176: step 13000, loss = 0.4607 (9258.4 examples/sec; 0.014 sec/batch)
	2019-05-09 07:47:50.741671: step 14000, loss = 0.4255 (9191.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:48:04.735264: step 15000, loss = 0.3311 (9147.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:48:18.584359: step 16000, loss = 0.2219 (9242.5 examples/sec; 0.014 sec/batch)
	2019-05-09 07:48:32.344847: step 17000, loss = 0.1758 (9302.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:48:46.137772: step 18000, loss = 0.3459 (9280.1 examples/sec; 0.014 sec/batch)
	2019-05-09 07:48:59.976843: step 19000, loss = 0.1121 (9249.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:49:13.965342: step 20000, loss = 0.1663 (9150.6 examples/sec; 0.014 sec/batch)
	2019-05-09 07:49:27.833051: step 21000, loss = 0.0902 (9229.9 examples/sec; 0.014 sec/batch)
	2019-05-09 07:49:41.754824: step 22000, loss = 0.0775 (9194.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:49:55.532559: step 23000, loss = 0.0596 (9290.4 examples/sec; 0.014 sec/batch)
	2019-05-09 07:50:09.440393: step 24000, loss = 0.0771 (9203.4 examples/sec; 0.014 sec/batch)
	2019-05-09 07:50:23.261574: step 25000, loss = 0.0417 (9261.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:50:37.097862: step 26000, loss = 0.0305 (9251.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:50:50.972016: step 27000, loss = 0.0403 (9225.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:51:05.038846: step 28000, loss = 0.0282 (9099.6 examples/sec; 0.014 sec/batch)
	2019-05-09 07:51:18.940243: step 29000, loss = 0.0207 (9207.5 examples/sec; 0.014 sec/batch)
	2019-05-09 07:51:32.957168: step 30000, loss = 0.0511 (9131.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:51:46.862366: step 31000, loss = 0.0173 (9205.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:52:00.782113: step 32000, loss = 0.0155 (9195.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:52:14.731469: step 33000, loss = 0.0296 (9175.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:52:28.611200: step 34000, loss = 0.0240 (9222.1 examples/sec; 0.014 sec/batch)
	2019-05-09 07:52:42.548172: step 35000, loss = 0.0216 (9184.2 examples/sec; 0.014 sec/batch)
	2019-05-09 07:52:56.568650: step 36000, loss = 0.0134 (9129.5 examples/sec; 0.014 sec/batch)
	2019-05-09 07:53:10.423532: step 37000, loss = 0.0153 (9238.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:53:24.505161: step 38000, loss = 0.0119 (9089.9 examples/sec; 0.014 sec/batch)
	2019-05-09 07:53:38.360910: step 39000, loss = 0.0120 (9237.8 examples/sec; 0.014 sec/batch)
	2019-05-09 07:53:52.291011: step 40000, loss = 0.0117 (9188.7 examples/sec; 0.014 sec/batch)
	2019-05-09 07:54:06.162870: step 41000, loss = 0.0125 (9227.5 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9750187546886722, 'APC': 0.8438155136268344, 'NOR': 0.9992012934393623}
	+p 	{'PVC': 0.981794833056353, 'APC': 0.8554729011689692, 'NOR': 0.9989727726610227}
Last iterations 41480
Re-train 1
---  training ---
	2019-05-09 07:54:59.267253: step 0, loss = 0.0108 (7898.8 examples/sec; 0.016 sec/batch)
	2019-05-09 07:55:13.422088: step 1000, loss = 0.0111 (9042.9 examples/sec; 0.014 sec/batch)
	2019-05-09 07:55:27.398744: step 2000, loss = 0.0105 (9157.9 examples/sec; 0.014 sec/batch)
	2019-05-09 07:55:41.490562: step 3000, loss = 0.0104 (9083.5 examples/sec; 0.014 sec/batch)
	2019-05-09 07:55:55.450185: step 4000, loss = 0.1066 (9169.1 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9816176470588235, 'APC': 0.8183246073298429, 'NOR': 0.9991004858152042}
	+p 	{'PVC': 0.9724245577523413, 'APC': 0.8875638841567292, 'NOR': 0.9990559399213348}
Last iterations 45628
Re-train 2
---  training ---
	2019-05-09 07:56:44.392270: step 0, loss = 0.0103 (7838.2 examples/sec; 0.016 sec/batch)
	2019-05-09 07:56:58.544061: step 1000, loss = 0.0110 (9044.7 examples/sec; 0.014 sec/batch)
	2019-05-09 07:57:12.511234: step 2000, loss = 0.0182 (9164.4 examples/sec; 0.014 sec/batch)
	2019-05-09 07:57:26.370834: step 3000, loss = 0.0197 (9235.5 examples/sec; 0.014 sec/batch)
	2019-05-09 07:57:40.202465: step 4000, loss = 0.0133 (9254.2 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.977794448612153, 'APC': 0.8983228511530398, 'NOR': 0.9984491134744898}
	+p 	{'PVC': 0.9791165865384616, 'APC': 0.7275042444821732, 'NOR': 0.9992821165532282}
Last iterations 49776
Re-train 3
---  training ---
	2019-05-09 07:58:29.154914: step 0, loss = 0.0135 (7889.1 examples/sec; 0.016 sec/batch)
	2019-05-09 07:58:43.370420: step 1000, loss = 0.0099 (9004.1 examples/sec; 0.014 sec/batch)
	2019-05-09 07:58:57.339037: step 2000, loss = 0.0144 (9163.4 examples/sec; 0.014 sec/batch)
	2019-05-09 07:59:11.352585: step 3000, loss = 0.0121 (9134.0 examples/sec; 0.014 sec/batch)
	2019-05-09 07:59:25.334888: step 4000, loss = 0.0108 (9154.4 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9705132052821128, 'APC': 0.839622641509434, 'NOR': 0.9992633317566049}
	+p 	{'PVC': 0.9874799603023131, 'APC': 0.8413865546218487, 'NOR': 0.9988121743410281}
Last iterations 53924
Re-train 4
---  training ---
	2019-05-09 08:00:14.003841: step 0, loss = 0.0100 (7961.9 examples/sec; 0.016 sec/batch)
	2019-05-09 08:00:28.023990: step 1000, loss = 0.0204 (9129.8 examples/sec; 0.014 sec/batch)
	2019-05-09 08:00:42.016349: step 2000, loss = 0.0108 (9147.6 examples/sec; 0.014 sec/batch)
	2019-05-09 08:00:55.915785: step 3000, loss = 0.0219 (9209.0 examples/sec; 0.014 sec/batch)
	2019-05-09 08:01:09.875970: step 4000, loss = 0.0109 (9168.9 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9750900360144058, 'APC': 0.8291404612159329, 'NOR': 0.9993059809706961}
	+p 	{'PVC': 0.9806821611832176, 'APC': 0.8947963800904978, 'NOR': 0.9988877089905669}
Last iterations 58072
Re-train 5
---  training ---
	2019-05-09 08:01:58.439289: step 0, loss = 0.0112 (8004.5 examples/sec; 0.016 sec/batch)
	2019-05-09 08:02:12.860557: step 1000, loss = 0.0094 (8875.8 examples/sec; 0.014 sec/batch)
	2019-05-09 08:02:26.881890: step 2000, loss = 0.0412 (9129.1 examples/sec; 0.014 sec/batch)
	2019-05-09 08:02:41.016272: step 3000, loss = 0.0096 (9056.0 examples/sec; 0.014 sec/batch)
	2019-05-09 08:02:55.118134: step 4000, loss = 0.0095 (9076.6 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9668317574666067, 'APC': 0.8596123624934521, 'NOR': 0.9993117981067663}
	+p 	{'PVC': 0.9869015702795864, 'APC': 0.838957055214724, 'NOR': 0.9988780396115963}
Last iterations 62220
Re-train 6
---  training ---
	2019-05-09 08:03:44.034550: step 0, loss = 0.0093 (7868.9 examples/sec; 0.016 sec/batch)
	2019-05-09 08:03:58.377888: step 1000, loss = 0.0092 (8924.2 examples/sec; 0.014 sec/batch)
	2019-05-09 08:04:12.449239: step 2000, loss = 0.0092 (9096.3 examples/sec; 0.014 sec/batch)
	2019-05-09 08:04:26.535942: step 3000, loss = 0.0478 (9086.6 examples/sec; 0.014 sec/batch)
	2019-05-09 08:04:40.576319: step 4000, loss = 0.0109 (9116.6 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9844676221205072, 'APC': 0.8611111111111112, 'NOR': 0.9986410435854737}
	+p 	{'PVC': 0.9642804645009555, 'APC': 0.8361323155216285, 'NOR': 0.9992919509370496}
Last iterations 66368
Re-train 7
---  training ---
	2019-05-09 08:05:29.704735: step 0, loss = 0.0109 (7917.2 examples/sec; 0.016 sec/batch)
	2019-05-09 08:05:44.070868: step 1000, loss = 0.1663 (8909.7 examples/sec; 0.014 sec/batch)
	2019-05-09 08:05:58.126589: step 2000, loss = 0.0130 (9106.8 examples/sec; 0.014 sec/batch)
	2019-05-09 08:06:12.237989: step 3000, loss = 0.0109 (9070.5 examples/sec; 0.014 sec/batch)
	2019-05-09 08:06:26.326940: step 4000, loss = 0.0129 (9085.3 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9846176934043671, 'APC': 0.8668763102725366, 'NOR': 0.9988445962581203}
	+p 	{'PVC': 0.969271679716354, 'APC': 0.8495120698510529, 'NOR': 0.999328920240425}
Last iterations 70516
Re-train 8
---  training ---
	2019-05-09 08:07:15.449626: step 0, loss = 0.0102 (7992.5 examples/sec; 0.016 sec/batch)
	2019-05-09 08:07:29.710317: step 1000, loss = 0.0511 (8975.7 examples/sec; 0.014 sec/batch)
	2019-05-09 08:07:43.514861: step 2000, loss = 0.0088 (9272.3 examples/sec; 0.014 sec/batch)
	2019-05-09 08:07:57.480460: step 3000, loss = 0.0179 (9165.2 examples/sec; 0.014 sec/batch)
	2019-05-09 08:08:11.494363: step 4000, loss = 0.0101 (9134.0 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9825219413397345, 'APC': 0.8485324947589099, 'NOR': 0.9989415178943573}
	+p 	{'PVC': 0.9733224344207476, 'APC': 0.8467573221757322, 'NOR': 0.9991933341477556}
Last iterations 74664
Re-train 9
---  training ---
	2019-05-09 08:09:00.360506: step 0, loss = 0.0102 (7976.0 examples/sec; 0.016 sec/batch)
	2019-05-09 08:09:14.565683: step 1000, loss = 0.0088 (9010.7 examples/sec; 0.014 sec/batch)
	2019-05-09 08:09:28.465266: step 2000, loss = 0.0323 (9208.9 examples/sec; 0.014 sec/batch)
	2019-05-09 08:09:42.426537: step 3000, loss = 0.0090 (9168.4 examples/sec; 0.014 sec/batch)
	2019-05-09 08:09:56.350920: step 4000, loss = 0.0082 (9192.3 examples/sec; 0.014 sec/batch)
---  validation ---
	se 	{'PVC': 0.9785446361590397, 'APC': 0.8602094240837697, 'NOR': 0.9992090448052855}
	+p 	{'PVC': 0.98237686398554, 'APC': 0.8526206538661131, 'NOR': 0.9991412515241331}
Last iterations 78812
---  testing full (mit+aha+esc+nst) ---
	se 	{'PVC': 0.9874126399639288, 'APC': 0.9207397622192867, 'NOR': 0.9994406838972215}
	+p 	{'PVC': 0.9894574343913551, 'APC': 0.8924455825864277, 'NOR': 0.9995036605037846}
	acc 	{'PVC': 0.9994209126792817, 'APC': 0.9993220441123297, 'NOR': 0.9989745917198988}
---  finish ---
