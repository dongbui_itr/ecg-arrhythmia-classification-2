import os
import shutil
import tensorflow as tf
import beat as model

'''
Loads the saved model, injects additional layers for the
input transformation and export the model into protobuf format
'''

# Command line arguments
tf.app.flags.DEFINE_string('output_dir', 'export_model',
                           """Directory where to export the model.""")
FLAGS = tf.app.flags.FLAGS


def main(argv=None):
    LEN_OF_FEATURE = 750
    VER = 4
    ARRHYTHMIA_CHECKPOINT = \
        '/media/dattran/MegaDataset/heartbeat_classification/200428/fullv24.A.e.i.1.2/checkpoint/beat2/best_new_metric'

    LABEL_ARRHYTHMIA = {"c": {"NOR": ["N", "L", "R"], "VES": ["V", "E"], "SVES": ["A", "a", "S", "J", "e", "j"]},
                        "d": {"NOR": ["N"], "B": ["L", "R"], "VES": ["V", "E"], "SVES": ["A", "a", "S", "J", "e", "j"]},
                        "e": {"NOR": ["N"], "L": ["L"], "R": ["R"], "VES": ["V", "E"],
                              "SVES": ["A", "a", "S", "J", "e", "j"]}}

    str_version = ARRHYTHMIA_CHECKPOINT.split('/')
    for i, v in enumerate(str_version):
        if v == "checkpoint":
            break

    VERSION = str_version[i - 1]
    MODEL_NAME = str_version[i + 1]
    CLASS = LABEL_ARRHYTHMIA[VERSION.split('.')[2]]
    serialized_tf_example = tf.placeholder(tf.float32,
                                           shape=(None, LEN_OF_FEATURE, 1),
                                           name='input_signal')
    name = MODEL_NAME + "_" + VERSION
    with tf.variable_scope(name, reuse=tf.AUTO_REUSE):
        logits = model.build(
            name=MODEL_NAME,
            inputs=serialized_tf_example,
            is_training=False,
            len_of_feature=LEN_OF_FEATURE,
            len_of_candidate_feature=LEN_OF_FEATURE,
            num_of_class=len(CLASS)
        )

        predictions = tf.argmax(logits, axis=1)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        # Restore the model from last checkpoints
        ckpt = tf.train.get_checkpoint_state(ARRHYTHMIA_CHECKPOINT)
        if ckpt and ckpt.model_checkpoint_path:
            restorer = tf.train.Saver()
            restorer.restore(sess, tf.train.latest_checkpoint(ARRHYTHMIA_CHECKPOINT))
        else:
            print("ERROR")

        # (re-)create export directory
        export_path = os.path.join(
            tf.compat.as_bytes(FLAGS.output_dir),
            tf.compat.as_bytes(str(VER)))
        if os.path.exists(export_path):
            shutil.rmtree(export_path)

        # create model builder
        builder = tf.saved_model.builder.SavedModelBuilder(export_path)

        # create tensors info
        predict_tensor_inputs = tf.saved_model.utils.build_tensor_info(serialized_tf_example)
        predict_tensor_outputs_classes = tf.saved_model.utils.build_tensor_info(predictions)
        # build prediction signature
        prediction_signature = (
            tf.saved_model.signature_def_utils.build_signature_def(
                inputs={'heartbeat': predict_tensor_inputs},
                outputs={'arrhythmia': predict_tensor_outputs_classes},
                method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME
            )
        )

        # save the model
        legacy_init_op = tf.group(tf.tables_initializer(), name='legacy_init_op')
        builder.add_meta_graph_and_variables(
            sess, [tf.saved_model.tag_constants.SERVING],
            signature_def_map={
                'arrhythmia_classification': prediction_signature
            },
            legacy_init_op=legacy_init_op)

        builder.save()

    print("Successfully exported BEAT model version {} into {}".format(name, FLAGS.output_dir))


if __name__ == '__main__':
    tf.app.run()
