import json
import os
import beat_input as data_model


def main():
    model_split_input = ['0.3.0.13']
    model_len_input = ['480.60.0.0']
    gamma = [0.0, 2.0, 3.0]
    models = ['beat_seq3']
    repeat = 3
    date = '210210'
    for s in model_split_input:
        for l in model_len_input:
            case = dict()
            for model in models:
                for g in gamma:
                    for i in range(repeat):
                        data_model_dir = 'data.{}.{}'.format(l, s)
                        version = 'beatv63subject.{}.{}.{}.{}'.format(l, s, int(g * 100), i)
                        key = version + '.' + model
                        assert key not in case.keys(), "{} already exists.!".format(key)
                        case[key] = dict()
                        case[key]["VERSION"] = version
                        case[key]["MODEL"] = model

                        case[key]["BEAT_BLOCK_LEN"] = int(version.split('.')[1])
                        case[key]["BEAT_SEGMENT_LEN"] = int(version.split('.')[2])
                        case[key]["BEAT_OVERLAP"] = float(version.split('.')[3]) / 100
                        case[key]["BEAT_CHANNEL_NUM"] = int(version.split('.')[4])
                        case[key]["BEAT_OFFSET_LEN"] = float(version.split('.')[5]) / 1000
                        case[key]["BEAT_FEATURE_LEN"] = int(version.split('.')[2]) * data_model.SAMPLING_RATE
                        case[key]["BEAT_BWR"] = int(version.split('.')[6])
                        case[key]["BEAT_NORM"] = bool(int(version.split('.')[7]))
                        case[key]["BEAT_CLASS"] = version.split('.')[8]
                        case[key]["GAMMA"] = float(version.split('.')[9]) / 100

                        case[key]["FUNCTION_CREATE_DATA"] = 'create_dataset_subject_based_tfrecord'
                        case[key]["DB_TRAINING"] = [
                                                    # ['ltdb', 'atr', 'atr'],
                                                    # ['stdb', 'atr', 'atr'],
                                                    # ['ltstdb', 'atr', 'atr'],
                                                    # ['svdb', 'atr', 'atr'],
                                                    # ['chfdb', 'ecg', 'ecg'],
                                                    # ['itrdb', 'itr', 'itr']
                                                    ['mitdb', 'atr', 'atr']
                                                    ]

                        case[key]["DB_TESTING"] = [
                                                   # ['ahadb', 'atr', 'atr'],
                                                   # ['escdb', 'atr', 'atr'],
                                                   ['mitdb', 'atr', 'atr']]

                        # case[key]["DB_TRAINING"] = [['mitdb_train', 'atr', 'atr']]
                        # case[key]["DB_TESTING"] = [['mitdb_valid', 'atr', 'atr']]

                        case[key]["CLASS"] = data_model.LABEL_ARRHYTHMIA[case[key]["BEAT_CLASS"]]
                        case[key]["CLASS_WEIGHT"] = [(1.0 + i) for i in range(len(
                            data_model.LABEL_ARRHYTHMIA[case[key]["BEAT_CLASS"]].keys()))]

                        case[key]["OUTPUT_DIR"] = '/{}/'.format(case[key]["VERSION"])
                        case[key]["DATA_MODEL_DIR"] = '/{}/'.format(data_model_dir)
                        case[key]["TRAIN_DIR"] = '/{}/train/'.format(data_model_dir)
                        case[key]["EVAL_DIR"] = '/{}/eval/'.format(data_model_dir)
                        case[key]["OUTPUT_EC57_LOSS"] = \
                            '/{}/ec57/{}/best_entropy_loss/'.format(case[key]["VERSION"],
                                                                    case[key]["MODEL"])
                        case[key]["OUTPUT_EC57_SQUARED_ERR"] = \
                            '/{}/ec57/{}/best_squared_err/'.format(case[key]["VERSION"],
                                                                   case[key]["MODEL"])
                        case[key]["OUTPUT_EC57_F1_SCORE"] = \
                            '/{}/ec57/{}/best_f1_score/'.format(case[key]["VERSION"],
                                                                case[key]["MODEL"])

            VERSION_CASES = '{}_{}.{}'.format(date, l, s)
            if not os.path.exists('cases/{}'.format(VERSION_CASES.split('_')[0])):
                os.makedirs('cases/{}'.format(VERSION_CASES.split('_')[0]))

            with open('cases/{}/{}.json'.format(VERSION_CASES.split('_')[0], VERSION_CASES), 'w') as fp:
                fp.write(json.dumps(case))
                fp.close()


if __name__ == "__main__":
    main()
