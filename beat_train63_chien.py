from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import csv
import json
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import re
import shutil
import socket
from os.path import basename, dirname
import tensorflow as tf
import beat_input as data_model
import train_utils
from utils.notiviaemail import NotiForm


os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = '0'
USE_GPU_INDEX = 0

FLAGS = tf.compat.v1.flags.FLAGS

tf.compat.v1.flags.DEFINE_integer('beat_block_len', 480, '')
tf.compat.v1.flags.DEFINE_integer('beat_segment_len', 60, '')
tf.compat.v1.flags.DEFINE_integer('beat_overlap', 0, '')
tf.compat.v1.flags.DEFINE_integer('beat_feature_len', 15360, '')
tf.compat.v1.flags.DEFINE_integer('beat_channel_num', 2, '')
tf.compat.v1.flags.DEFINE_float('beat_offset_len', 0.040, 'ms')
tf.compat.v1.flags.DEFINE_integer('beat_bwr', 0, '')
tf.compat.v1.flags.DEFINE_boolean('beat_norm', True, '')
tf.compat.v1.flags.DEFINE_string('beat_class', "0", '')
tf.compat.v1.flags.DEFINE_integer('beat_class_num', 2, '')

# environment
# ENV_MEDIA_PATH_VERSION = '/home/dattran/MegaDataset'
# ENV_PATH_DATA_TRAINING = '/home/dattran/PhysionetData'
ENV_MEDIA_PATH_VERSION = '/mnt/ai_data/MegaDataset'
ENV_PATH_DATA_TRAINING = '/mnt/ai_data/PhysionetData'


# ENV_MEDIA_PATH_VERSION = os.environ['ENV_MEDIA_PATH_VERSION']
# ENV_PATH_DATA_TRAINING = os.environ['ENV_PATH_DATA_TRAINING']


def build_logfile_name(dir_checkpoint, name):
    """Generate logfile name based on training configuration and model params"""
    return '{}checkpoint/{}'.format(dir_checkpoint, name)


def build_save_path(dir_checkpoint, name, version):
    """Generate logfile name based on training configuration and model params"""
    return '{}save/{}'.format(dir_checkpoint, name)


def main(argv=None):
    """

    :param argv:
    :return:
    """
    SAVE_DISK_MEMORY = False
    MAX_EPOCH = 150
    # BATCH_SIZE_TRAINING = data_model.BATCH_SIZE
    BATCH_SIZE_TRAINING = 48
    CASES_PATH = ['cases/210210/210210_480.60.0.0.0.3.0.13.json']

    for CASE_PATH in CASES_PATH:
        DIR_CASE = basename(CASE_PATH).split('_')[0]

        MEDIA_PATH_VERSION = ENV_MEDIA_PATH_VERSION + '/beat_classification/{}'.format(DIR_CASE)
        PATH_DATA_TRAINING = ENV_PATH_DATA_TRAINING + '/{}'

        if os.path.exists(CASE_PATH):
            with open(CASE_PATH) as json_file:
                case = json.load(json_file)
                for n, k in enumerate(case.keys()):
                    print("+++++++++++++++++++++++++++++++" + "[{}/{}] ".format(n + 1, len(case.keys())) +
                          case[k]["VERSION"] + "+++++++++++++++++++++++++++++++\n\n")
                    tf.compat.v1.reset_default_graph()
                    tf.keras.backend.clear_session()
                    FLAGS.beat_block_len = case[k]["BEAT_BLOCK_LEN"]
                    FLAGS.beat_segment_len = case[k]["BEAT_SEGMENT_LEN"]
                    FLAGS.beat_overlap = case[k]["BEAT_OVERLAP"]
                    FLAGS.beat_channel_num = case[k]["BEAT_CHANNEL_NUM"]
                    FLAGS.beat_offset_len = case[k]["BEAT_OFFSET_LEN"]
                    FLAGS.beat_feature_len = case[k]["BEAT_FEATURE_LEN"]
                    FLAGS.beat_bwr = case[k]["BEAT_BWR"]
                    FLAGS.beat_norm = case[k]["BEAT_NORM"]
                    FLAGS.beat_class = case[k]["BEAT_CLASS"]
                    FLAGS.beat_class_num = len(data_model.LABEL_ARRHYTHMIA[case[k]["BEAT_CLASS"]].keys())

                    if not os.path.exists('{}/num_samples.txt'.format(MEDIA_PATH_VERSION + case[k]["DATA_MODEL_DIR"])):
                        getattr(data_model, case[k]["FUNCTION_CREATE_DATA"])(
                            use_gpu_index=USE_GPU_INDEX,
                            data_model_dir=MEDIA_PATH_VERSION + case[k]["DATA_MODEL_DIR"],
                            train_dir=MEDIA_PATH_VERSION + case[k]["TRAIN_DIR"],
                            eval_dir=MEDIA_PATH_VERSION + case[k]["EVAL_DIR"],
                            data_dir=PATH_DATA_TRAINING,
                            db_train=case[k]["DB_TRAINING"],
                            db_eval=case[k]["DB_TESTING"],
                            save_image=False)

                    # if not os.path.exists('{}/num_samples.txt'.format(MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"])):
                    from shutil import copyfile
                    if not os.path.exists(MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"]):
                        os.makedirs(MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"])

                    copyfile('{}/num_samples.txt'.format(MEDIA_PATH_VERSION + case[k]["DATA_MODEL_DIR"]),
                             '{}/num_samples.txt'.format(MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"]))

                    checkpoint_dir = build_logfile_name(MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"], case[k]["MODEL"])
                    if not os.path.exists(checkpoint_dir + '/last/checkpoint'):
                        train_utils.train_beat(use_gpu_index=USE_GPU_INDEX,
                                               name=case[k]["MODEL"],
                                               ver=case[k]["VERSION"],
                                               output_dir=MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"],
                                               model_dir=checkpoint_dir,
                                               lbl_train=data_model.LABEL_ARRHYTHMIA[case[k]["BEAT_CLASS"]],
                                               class_weights=case[k]["CLASS_WEIGHT"],
                                               gamma=case[k]["GAMMA"],
                                               resume_from=None,
                                               train_directory=MEDIA_PATH_VERSION + case[k]["TRAIN_DIR"],
                                               eval_directory=MEDIA_PATH_VERSION + case[k]["EVAL_DIR"],
                                               batch_size=BATCH_SIZE_TRAINING,
                                               epoch_num=MAX_EPOCH)

                    else:
                        file_checkpoint = open(checkpoint_dir + '/last/checkpoint', "r")
                        line_checkpoint = file_checkpoint.readlines()
                        begin_at_epoch = line_checkpoint[-1].split('/')[-1][:-2]

                        if int(re.findall(r'\d+', begin_at_epoch)[-1]) < MAX_EPOCH:
                            train_utils.train_beat(use_gpu_index=USE_GPU_INDEX,
                                                   name=case[k]["MODEL"],
                                                   ver=case[k]["VERSION"],
                                                   output_dir=MEDIA_PATH_VERSION + case[k]["OUTPUT_DIR"],
                                                   model_dir=checkpoint_dir,
                                                   lbl_train=data_model.LABEL_ARRHYTHMIA[case[k]["BEAT_CLASS"]],
                                                   class_weights=case[k]["CLASS_WEIGHT"],
                                                   gamma=case[k]["GAMMA"],
                                                   resume_from=checkpoint_dir + '/last/' + begin_at_epoch,
                                                   train_directory=MEDIA_PATH_VERSION + case[k]["TRAIN_DIR"],
                                                   eval_directory=MEDIA_PATH_VERSION + case[k]["EVAL_DIR"],
                                                   batch_size=BATCH_SIZE_TRAINING,
                                                   epoch_num=MAX_EPOCH - int(re.findall(r'\d+', begin_at_epoch)[-1]))

                    tf.compat.v1.reset_default_graph()
                    tf.keras.backend.clear_session()
                    if SAVE_DISK_MEMORY:
                        if os.path.exists(MEDIA_PATH_VERSION + case[k]["TRAIN_DIR"]):
                            shutil.rmtree(MEDIA_PATH_VERSION + case[k]["TRAIN_DIR"])

                        if os.path.exists(MEDIA_PATH_VERSION + case[k]["EVAL_DIR"]):
                            shutil.rmtree(MEDIA_PATH_VERSION + case[k]["EVAL_DIR"])

        else:
            print("{} does not exist.\n".format(CASE_PATH))

    case_noti = ""
    for c in CASES_PATH:
        case_noti += basename(c) + ", "

    # noti = \
    #     NotiForm("[{}]Noti from {}".format(socket.gethostname(), case_noti),
    #              "Hello {},\n"
    #              "Your program has finished, please teamview to your computer to check.\n"
    #              "Thanks,".format("DatTran"))
    # noti.send_email('dattran@itrvn.com')


if __name__ == '__main__':
    tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)
    main()
